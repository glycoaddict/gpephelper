import re

filepath = r'C:\Users\revit\Google Drive\Postdoc\502 IVG Paper JASMS\03e revision 3\byonic legend.txt'

with open(filepath, 'r') as f:
    txt = f.readlines()

txt = [x.strip().replace('\'','') for x in txt if len(x.strip())>0]

print(txt)

r = txt[0]

result_list = []

for i, r in enumerate(txt):
    print('index = {}\nr = {}'.format(i,r))
    ans_dict = {}

    ans_dict['pep'] = re.findall('(^[A-Z]+) ', r)[0]
    ans_dict['glycan'] = r''.join(re.findall(r'([FHNS]\d)', r))
    ans_dict['neutral'] = re.findall(r'(\d{3,4}\.\d+) Da', r)[0]
    ans_dict['precursor'], ans_dict['charge'] = re.findall(r'(\d{3,4}\.\d+)\((\d)\)', r)[0]
    ans_dict['rt'] = re.findall(r'(\d{3,4}\.\d+)s', r)[0]
    ans_dict['mode'] = re.findall(r'HCD|ETD|ETHCD', r)[0]

    result = 'MS/MS {mode} spectra of the glycopeptide {pep} with glycan composition {glycan}, eluting at {rt}s with precursor m/z {precursor}(+{charge}) and neutral mass of {neutral} Da.'.format(**ans_dict)

    result_list.append(result)
    result_list.append('\n')

with open('answer.txt', 'w+') as f:
    f.writelines(result_list)
    f.close()

